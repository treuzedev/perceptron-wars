# IMPORTS
import json as json
import random as random
import sys as sys
import numpy as numpy
from time import strftime


# CROSS ENTROPY
class CrossEntropyCost(object):

    # get function name
    @staticmethod
    def getName():
        return 'cross-entropy'

    # output layer cost function
    # for one training set!
    @staticmethod
    def cost(yHat, y):
        a = y * numpy.log(yHat)
        b = (1 - y) * numpy.log(1 - yHat)
        c = sum(a + b)
        return (-c)

    # output layer error
    @staticmethod
    def outputLayerError(yHat, y, z=None):
        """
        z is not used! it here so that the model works for both cot functions easily.
        """

        # return error
        return yHat - y

    # hidden layers error
    @staticmethod
    def hiddenLayersError(forwardLayerErrorWeights, forwardLayerError, z):
        a = numpy.dot(forwardLayerErrorWeights.transpose(), forwardLayerError)
        return a * CrossEntropyCost.sigmoidDerivative(z)

    # sigmoid derivative
    @staticmethod
    def sigmoidDerivative(z):
        return CrossEntropyCost.sigmoid(z) * (1 - CrossEntropyCost.sigmoid(z))

    # sigmoid
    @staticmethod
    def sigmoid(z):
        a = 1.0 + numpy.exp(-z)
        return (1.0 / a)

    # loss
    @staticmethod
    def loss(yHat, y):
        return yHat - y


# QUADRATIC
class QuadraticCost(object):

    # get function name
    @staticmethod
    def getName():
        return 'quadratic'

    # output layer cost
    # for one training set!
    @staticmethod
    def cost(yHat, y):
        a = numpy.linalg.norm(yHat - y)
        b = a**2
        return (0.5 * b)

    # output layer error
    @staticmethod
    def outputLayerError(yHat, y, z):
        return (yHat - y) * QuadraticCost.sigmoidDerivative(z)

    # hidden layers error
    @staticmethod
    def hiddenLayersError(forwardLayerErrorWeights, forwardLayerError, z):
        a = numpy.dot(forwardLayerErrorWeights, forwardLayerError)
        return a * CrossEntropyCost.sigmoidDerivative(z)

    # sigmoid derivative
    @staticmethod
    def sigmoidDerivative(z):
        return QuadraticCost.sigmoid(z) * (1 - QuadraticCost.sigmoid(z))

    # sigmoid
    @staticmethod
    def sigmoid(z):
        a = 1.0 + numpy.exp(-z)
        return 1.0 / a

    # loss
    @staticmethod
    def loss(yHat, y):
        return yHat - y


# NEURAL NETWORK DEFINITION
class PerceptronNetwork(object):

    # CONSTRUCTOR
    def __init__(self, sizes, cost=CrossEntropyCost()):
        self.numberOfLayers = len(sizes)
        self.sizes = sizes
        self.defaultInit()
        self.cost = cost

    # DEFAULT WEIGHTS AND BIASES INITIALIZER
    # initialize weights with smaller values
    def defaultInit(self):

        # create a vertical array of biases, one for every layer
        # every value is a bias for a neuron
        self.biases = [numpy.random.randn(n, 1) for n in self.sizes[1:]]

        # create a matrix of weights, one for every layer
        # every row is the weights of a neuron in l
        # every column is the number of neuron in (l - 1)
        self.weights = [numpy.random.randn(i, j) / numpy.sqrt(j)
                        for i, j in zip(self.sizes[1:], self.sizes[:-1])]

        # return true if everything is okay
        return True

    # BIG WEIGHTS AND BIASES VALUES INITIALIZER
    def bigValuesInit(self):

        # create a vertical array of biases, one for every layer
        # every value is a bias for a neuron
        self.biases = [numpy.random.randn(n, 1) for n in self.sizes[1:]]

        # create a matrix of weights, one for every layer
        # every row is the weights of a neuron in l
        # every column is the number of neuron in (l - 1)
        self.weights = [numpy.random.randn(i, j)
                        for i, j in zip(self.sizes[1:], self.sizes[:-1])]

        # return true if everything is okay
        return True

    # FEEDFORWARD
    def feedForward(self, inputs):

        # loop through all layers
        for b, w in zip(self.biases, self.weights):
            inputs = self.cost.sigmoid(numpy.dot(w, inputs) + b)

        # return output layer activations
        return inputs

    # BACKPROPAGATION
    def backPropagation(self, x, y):

        # create variables to store new data
        newBiases = [numpy.zeros(b.shape) for b in self.biases]
        newWeights = [numpy.zeros(w.shape) for w in self.weights]

        # feed forward
        # the first layer activation values are the inputs
        activation = x
        activationsByLayer = [x]
        zVector = []

        # loop through all layers to get the activation values
        for b, w in zip(self.biases, self.weights):
            z = numpy.dot(w, activation) + b
            zVector.append(z)
            activation = self.cost.sigmoid(z)
            activationsByLayer.append(activation)

        # backpropagate the error for the outer layer
        outerLayerError = self.cost.outputLayerError(
            activationsByLayer[-1], y, zVector[-1])
        newBiases[-1] = outerLayerError
        newWeights[-1] = numpy.dot(outerLayerError,
                                   activationsByLayer[-2].transpose())
        forwardLayerError = outerLayerError

        # backpropagate the error for the other layers
        for layer in range(2, self.numberOfLayers):

            # calculate the errorr
            hiddenLayerError = self.cost.hiddenLayersError(
                self.weights[(-layer + 1)], forwardLayerError, zVector[-layer])

            # store it for next iteration
            forwardLayerError = hiddenLayerError

            # calculate rate of change
            newBiases[-layer] = hiddenLayerError
            newWeights[-layer] = numpy.dot(hiddenLayerError,
                                           activationsByLayer[-layer - 1].transpose())

        # return rate of change for weights and biases
        # they are updated in a different function
        return newBiases, newWeights

    # TRAIN MODEL
    def train(self,
              trainData,
              epochs=50,
              batchSize=10,
              learningRate=3.0,
              lmbda=5.0,
              validationData=None):

        # get train data size
        trainSize = len(trainData)

        # run epochs
        for epoch in range(epochs):

            # shuffle data
            random.shuffle(trainData)

            # create batches
            batches = [trainData[k:(k + batchSize)]
                       for k in range(0, trainSize, batchSize)]

            # train
            for batch in batches:

                # update biases and weights
                self.updatesPerBatch(batch, learningRate, lmbda, trainSize)

            # get accuracy
            trainAccuracy = self.evaluateAccuracy(trainData)
            trainCost = self.evaluateCost(trainData)

            # log statements
            print(f'Epoch: {epoch}')
            print('\nTRAIN DATA')
            print(f'Accuracy: {round(trainAccuracy * 100, 2)}')
            print(f'Cost: {round(trainCost, 2)}')

            # log if there is validation data
            if validationData:

                # get metrics
                validationAccuracy = self.evaluateAccuracy(validationData)
                validationCost = self.evaluateCost(trainData)

                # logs
                print('\nVALIDATION DATA')
                print(f'Accuracy: {round(validationAccuracy * 100, 2)}')
                print(f'Cost: {round(validationCost, 2)}')

            # separate epochs
            print('\n-----------\n')

        # when training completes
        print('Training Completed!')

        # return when train finishes
        return True

    # UPDATE MODEL PER BATCH
    def updatesPerBatch(self, batch, learningRate, lmbda, trainSize):

        # create variables to store new values
        newBiases = [numpy.zeros(b.shape) for b in self.biases]
        newWeights = [numpy.zeros(w.shape) for w in self.weights]

        # backpropagate the error
        for x, y in batch:

            # backpropagate
            propBiases, propWeights = self.backPropagation(x, y)

            # store values
            newBiases = [a + b for a, b in zip(newBiases, propBiases)]
            newWeights = [a + b for a, b in zip(newWeights, propWeights)]

        # update weights and biases
        rate = learningRate / trainSize
        lambdaRate = lmbda / trainSize
        self.biases = [a - rate * b for a, b in zip(self.biases, newBiases)]
        self.weights = [a * (1 - rate * lambdaRate) - rate *
                        b for a, b in zip(self.weights, newWeights)]

        # return if okay
        return True

    # EVALUATE ACCURACY
    def evaluateAccuracy(self, testData):

        # get model results
        testResults = [(numpy.argmax(self.feedForward(x)), y)
                       for (x, y) in testData]

        # compare results
        compare = [item[1][item[0]] == 1 for item in testResults] * 1
        compare = sum(compare)

        # return the number of correct predictions
        return compare[0] / len(testData)

    # EVALUATE COST
    def evaluateCost(self, testData):

        totalCost = [self.cost.cost(self.feedForward(x), y)
                     for (x, y) in testData]
        totalCost = sum(totalCost)
        return totalCost[0] / len(testData)

    # SAVE MODEL
    def saveModel(self, folder):

        # get filename
        filename = folder + f'model{strftime("%H%M")}.json'

        # transform model into json
        data = {'sizes': self.sizes,
                'weights': [w.tolist() for w in self.weights],
                'bias': [b.tolist() for b in self.biases],
                'cost': self.cost.getName()}

        # create a file
        file = open(filename, 'w')

        # write to file
        json.dump(data, file)

        # log
        print('Model saved at ->', filename)

        # close connection
        file.close()

    # LOAD A MODEL
    @staticmethod
    def loadModel(path):

        # open file
        file = open(path, 'r')

        # load model
        modelAsJson = json.load(file)

        # close connection
        file.close()

        # get model parameters
        cost = modelAsJson['cost']
        sizes = modelAsJson['sizes']

        # create a variable for later use
        model = True

        # define cost function
        if cost == 'cross-entropy':
            model = PerceptronNetwork(sizes)
        else:
            model = PerceptronNetwork(sizes, cost=QuadraticCost)

        # update weights and biases
        model.weights = [numpy.array(w) for w in modelAsJson['weights']]
        model.biases = [numpy.array(b) for b in modelAsJson['bias']]

        # log and return model
        print('Model loaded from ->', path)
        return model

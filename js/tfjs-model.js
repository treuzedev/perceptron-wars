// MAKE PREDICTIONS
async function makeTFJSPrediction(image) {

    // variables
    const modelPath = 'models/tf-model/tfjs-model/model.json';
    const model = await tf.loadGraphModel(modelPath);
    const xTensor = tf.tensor([image]);

    // there is a problem with the conversion
    // use a execute method, feeding the inputs as a dictionary and the outputs names as a list
    const outputs = await model.execute({
        'xTensor': xTensor
    },
        ['accuracy/modelPrediction:0']);

    // get value from tensor
    // since we only have one valuee, we can get it straight away
    const result = outputs.dataSync()[0];

    // cleanup
    outputs.dispose();
    xTensor.dispose();

    // return
    return result;
};